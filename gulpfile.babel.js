import gulp from 'gulp';

import del from 'del';

import less from 'gulp-less';
import LessNpmImportPlugin from 'less-plugin-npm-import';

// Custom compile bootstrap
gulp.task(
  'css:clean:bootstrap', () =>
    del('./build/dist/css/bootstrap.css'));

gulp.task(
  'css:compile:bootstrap',
  ['css:clean:bootstrap'],
  () =>
    gulp.src('styles/bootstrap.less')
    .pipe(less({
      plugins: [
        new LessNpmImportPlugin({
          prefix: '~',
        }),
      ],
    }))
    .pipe(gulp.dest('build/dist/css')));
