var fetch = require('node-fetch'); // eslint-disable-line no-var
require('babel-register');
require('css-modules-require-hook')({
  generateScopedName: '[name]__[local]' + (process.env.NODE_ENV === 'production' ? '-[hash:base64:4]' : ''), // eslint-disable-line prefer-template, max-len
  mode: 'local',
  rootDir: './lib/modules',
});

global.fetch = fetch;
global.Response = fetch.Response;
global.Headers = fetch.Headers;
global.Request = fetch.Request;

require('./lib/server');

