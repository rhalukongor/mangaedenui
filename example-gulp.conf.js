import nopt from 'nopt';
import path from 'path';

import es from 'event-stream';
import streamQueue from 'streamqueue';

import gulp from 'gulp';
import sourceStream from 'vinyl-source-stream2';
import buffer from 'vinyl-buffer';
import lazyPipe from 'lazypipe';
import uglify from 'gulp-uglify';
import babel from 'gulp-babel';
import rev from 'gulp-rev';
import sourcemaps from 'gulp-sourcemaps';
import iff from 'gulp-if';
import cache from 'gulp-cached';
import remember from 'gulp-remember';
import server from 'gulp-develop-server';
import concat from 'gulp-concat';

import browserify from 'browserify-incremental';
import babelify from 'babelify';
import envify from 'envify';
import brfs from 'brfs';

import postcss from 'gulp-postcss';
import calc from 'postcss-calc';
import cssModulesify from 'css-modulesify';
import genericNames from 'generic-names';
import autoprefixer from 'autoprefixer';
import doiuse from 'doiuse';
import csswring from 'csswring';

import browserSync from 'browser-sync';

import fs from 'fs';
import del from 'del';
import through from 'through2';

import glob from 'glob-promise';
import parseJSON from 'json-parse-stream';

// import curry from 'ramda/src/curry';

const generateScopedName = genericNames('[name]__[local]__[hash:base64:5]', {
  context: path.join(__dirname, 'src')
});

const options = Object.assign({
  env: process.env.NODE_ENV || 'production',
  outdir: 'build'
}, nopt({
  env: String,
  verbose: Boolean,
  outdir: path,
  doiuse: Boolean
}, {
  d: ['--env', 'development'],
  v: ['--verbose']
}));

process.env.NODE_ENV = options.env;

(function(defineOption) {
  defineOption('isDev', { get: (function() {
    return this.env === 'development';
  }).bind(options) });
}(Object.defineProperty.bind(null, options)));

function doIUseConsoleReporter({ featureData }) {
  const links = featureData.caniuseData.links.map(link => `\n\t${link.url}`);
  console.warn(
    `CSS Feature ${featureData.title} is missing from browser ${featureData.missing}.\n`,
    `See: ${links} for more information...`);
}

const log = options.debug ? console.log.bind(console) : () => {};
log('Starting gulp with options:', options);

gulp.task('clean:js:client', function() {
  const staticDir = `${options.outdir}/static/`;
  return del([`${staticDir}/bundle*.js`, `${staticDir}/jigle*.css`]);
});

gulp.task('bundle:js:client', ['copy-config'], function() {
  const additionalPostCssPlugins = [];
  if(options.doiuse) {
    additionalPostCssPlugins.push(doiuse({
      browsers: ['ie >= 9', '> 1%'],
      onFeatureUsage: doIUseConsoleReporter
    }));
  }

  const sourcePath = path.join(__dirname, 'src');

  function sourceProps(fileName) {
    const staticPath = path.join(__dirname, 'static', fileName);
    return { path: staticPath, base: __dirname };
  }

  const jigleCssStream = lazyPipe()
          .pipe(sourceStream, sourceProps('jigle.css'))();

  const baseCssStream = gulp.src(['assets/css/base.css',
                                  'assets/css/icons.css',
                                  'node_modules/normalize.css/normalize.css'])
          .pipe(cache('base-css'));

  const cssStream = streamQueue.obj(baseCssStream, jigleCssStream)
          .pipe(iff(options.isDev, sourcemaps.init()))
          .pipe(postcss([
            calc({ warnWhenCannotResolve: true }),
            csswring
          ]))
          .pipe(remember('base-css'))
          .pipe(concat(sourceProps('jigle.css')));

  const jsStream =
        browserify('src/client.jsx', {
          extensions: ['.js', '.jsx', '.json'],
          debug: options.isDev
          // cacheFile: 'build/.browserify-cache.json'
          // cache: {},
          // packageCache: {},
          // fullPaths: true
        })
        .transform(babelify, {
          sourceMapRelative: sourcePath
        })
        .transform(envify, {
          NODE_ENV: options.env,
          RUNTIME: 'client'
        })
        .plugin(cssModulesify, {
          rootDir: sourcePath,
          generateScopedName,
          after: [
            autoprefixer({
              browsers: ['ie >= 9', '> 1%']
            })
          ].concat(additionalPostCssPlugins)
        })
        .transform(brfs)
        .on('css stream', stream => {
          stream.pipe(jigleCssStream);
        })
        .bundle()
        .on('error', console.error.bind(console, '[Error]'))
        .pipe(iff(
          options.isDev,
          sourceStream(sourceProps('bundle.js'))))
        .pipe(iff(options.isDev, buffer()))
        .pipe(iff(options.isDev, sourcemaps.init({ loadMaps: true })))
        .pipe(iff(!options.isDev, uglify()));

  return es
    .merge(jsStream, cssStream)
    .pipe(rev())
    .pipe(iff(options.isDev, sourcemaps.write('./')))
    .pipe(gulp.dest(options.outdir))
    .pipe(rev.manifest({ path: `${ options.outdir }/rev-manifest.json`,
                         base: options.outdir,
                         merge: true }))
    .pipe(gulp.dest(options.outdir));
});

gulp.task('clean:config', function() {
  return del(`${options.outdir}/server/config/**`);
});

gulp.task('copy-config', ['clean:config'], function() {
  let streams = [];
  const outdir = `${options.outdir}/server/config`;

  streams.push(
    gulp.src('config/*.js')
      .pipe(babel())
      .pipe(gulp.dest(outdir)));

  streams.push(
    gulp.src(`config/${options.env}.json`)
      .pipe(gulp.dest(outdir))
  );

  return es.merge(streams);
});

gulp.task('clean:js:server', function() {
  let output = `${options.outdir}/server`;
  return del([`${output}/**/*`, `!${output}/config`, `!${output}/config/*`]);
});

gulp.task('bundle:js:server:compile', ['clean:js:server'], function() {
  let compile6 = lazyPipe()
        .pipe(() => iff(options.isDev, sourcemaps.init()))
        .pipe(babel, {
          plugins: [
            ['css-modules-transform', {
              generateScopedName
            }]]
        })
        .pipe(() => iff(options.isDev, sourcemaps.write()));

  let streams = [];

  streams.push(
    gulp
      .src(['src/**/*.js', 'src/**/*.jsx'])
      .pipe(cache('server-scripts'))
      // .pipe(require('gulp-debug')({ minimal: false }))
      .pipe(compile6())
      .pipe(remember('server-scripts'))
      .pipe(gulp.dest(`${options.outdir}/server/src`)));

  streams.push(
    gulp
      .src('index.js')
      .pipe(compile6())
      .pipe(gulp.dest(`${options.outdir}/server/`)));

  return es.merge(streams);
});

gulp.task('clean:messages', function() {
  return del(`${options.outdir}/static/i18n/**`);
});

gulp.task('bundle:messages', function() {
  function mapLocale(locale) {
    const sourceDir = './i18n';
    return {
      name: locale,
      sourceGlob: `${sourceDir}/${locale}/**/*.json`
    };
  }

  function filterObjects(e, enc, done) {
    // mode == 4 when parent is a value array
    if(e.type === 'object' && e.parent && e.parent.mode === 4) {
      this.push(e.value);
      done();
    } else {
      done();
    }
  }

  function mapStream(locale) {
    const encoding = 'utf8';
    return glob(locale.sourceGlob)
      .then(paths => paths.map(
        path =>
          fs.createReadStream(path, { encoding })
          .pipe(parseJSON())
          .pipe(through.obj(filterObjects))
      ))
      .then(streams => {
        return { locale, streams };
      });
  }

  function processMessage() {
    let firstChunk = true;
    return through.obj(function({ id, defaultMessage }, enc, done) {
      if(firstChunk) {
        this.push(new Buffer('{'));
      }
      if(id && defaultMessage) {
        let maybeComma = firstChunk ? '' : ',';
        this.push(new Buffer(`${maybeComma}"${id}":"${defaultMessage}"`));
      }
      firstChunk = false;
      done();
    }, function(done) {
      this.push(new Buffer('}'));
      done();
    });
  }

  function mapWriteStream(promise) {
    const base = process.cwd();
    return promise.then(
      ({ locale, streams }) =>
        es.merge(streams)
        .pipe(processMessage())
        .pipe(sourceStream({ base, path: `static/i18n/${locale.name}.json` }))
        .pipe(rev())
        .pipe(gulp.dest(`${options.outdir}`))
    );
  }

  return Promise.all(
    ['en', 'en-GB', 'tr']
      .map(mapLocale)
      .map(mapStream)
      .map(mapWriteStream))
    .then(
      streams =>
        es.merge(streams)
        .pipe(rev.manifest({ path: `${ options.outdir }/rev-manifest.json`,
                             base: options.outdir,
                             merge: true }))
        .pipe(gulp.dest(options.outdir)))
    .then(
      stream => new Promise(
        (resolve, reject) => {
          stream.on('error', reject);
          stream.on('end', resolve);
        }
      )
    );
});

gulp.task('copy:deprecated-assets', function() {
  return gulp.src('assets/**/*')
    .pipe(gulp.dest('build/static'));
});

gulp.task('bundle:js:server', ['copy-config', 'bundle:js:server:compile']);

gulp.task('bundle:js', ['bundle:js:client', 'bundle:js:server']);

gulp.task('bundle', ['bundle:js', 'bundle:messages']);

gulp.task('clean', ['clean:js:client', 'clean:js:server', 'clean:config', 'clean:messages']);

gulp.task('serve:start', ['clean', 'copy:deprecated-assets', 'bundle'], function() {
  server.listen({
    path: 'build/server/index.js',
    execArgv: ['-r', 'source-map-support/register'],
    delay: 1800
  }, function() {
    browserSync({
      proxy: {
        target: 'http://jigle.dev:3000'
      },
      port: 3001
    });
  });
});

gulp.task('serve:restart', ['bundle'], function(done) {
  server.restart(error => {
    if(!error) {
      setTimeout(() => {
        browserSync.reload();
        done();
      }, 1800);
    } else {
      done();
    }
  });
});

gulp.task('serve', ['serve:start'], function() {
  gulp.watch(['index.js', 'src/**/*'], ['serve:restart']);
});
