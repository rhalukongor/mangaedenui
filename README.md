MangaEden UI
============

### About
MangaEden UI is a very simple Manga Reader which utilizes MangaEden API. I've done this as a hiring challenge and to try a few things like Webpack's hot module replacement hence I was allocating time.

### Features
* A [universal (Isomorphic)](http://isomorphic.net/) [React](https://facebook.github.io/react/) application
* Uses the excellent [Redux](http://redux.js.org/) framework
* Written in [ES6](http://es6-features.org/) and transpiled by [Babel](https://babeljs.io/)
* Responsive thanks to [Bootstrap](https://getbootstrap.com/)
* Styleable and maintainable. Long live [css modules](https://github.com/css-modules/css-modules)
* Shareable Chapter/Page URL's 

### Install

First install global commands if you don't already have them:

```sh
npm i -g gulp-cli nodemon
```

Install dependencies

```sh
npm install 
```

Now to compile bootstrap:

```sh
npm run build-bs 
```

We're ready. Start the server and navigate to [http://localhost:3000](http://localhost:3000):

```sh
npm run start-dev
```

Run tests:

```sh
npm test
```

Run linter: (If it doesn't flood output, it is a good thing)

```sh
npm run lint
```

### FAQ

**Why don't you use Bower**

Bower is not a good a fit for universal apps and [officially discouraged](https://webpack.github.io/docs/usage-with-bower.html) by many tools like this project's build tool; Webpack. Many tools work with Npm without any configuration. E.g: The less import plugin I've used for compiling a custom Bootstrap

**Gulp and Webpack: That's two build tools!**

My first intention was to use WebPack for javascript compilation only but WebPack took all the burden without me knowing. This was the first time I was using WebPack; it looks like less configuration (a lot less comparing with Gulp) and less control. Hot module replacement is nice when it works and it doesn't work all the time. I've added an `example-gulp.conf.js` from another project of mine to the root of project, to demonstrate what can be done.

**You don't use Sass or Less**

I was hoping to try the level of PostCSS integration with Less and Sass; there are many utilities out there claiming variable and function import. Unfortunately css modules leave so little to be desired. There was only one place that I wanted to use a variable from bootstrap and it would be an overkill to add an additional processing step for a single variable import.

**Not everything is tested**

Tests are examplary. Imagine more of the same...

### License
[WTFPL](http://www.wtfpl.net/)
