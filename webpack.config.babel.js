import AssetsPlugin from 'assets-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import autoprefixer from 'autoprefixer';
import path from 'path';
import webpack from 'webpack';

const DEBUG = process.env.NODE_ENV !== 'production';
const APPEND_HASH = DEBUG ? '' : '-[hash:base64:4]';

export default {
  entry: DEBUG ? [
    'webpack-hot-middleware/client',
    './index.jsx',
  ] : './index.jsx',
  context: path.resolve(__dirname, './lib/client'),
  resolve: {
    extensions: ['', '.js', '.jsx'],
  },
  output: {
    filename: `[name]${DEBUG ? '' : '.[hash]'}.js`,
    hashDigestLength: 7,
    path: path.resolve(__dirname, './build/dist'),
    publicPath: '/',
  },
  module: {
    loaders: [
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract(
          'style',
          `css?modules&importLoaders=1&localIdentName=[name]__[local]${APPEND_HASH}!postcss`),
        exclude: /node_modules/,
      },
      {
        test: /\.jsx?$/,
        loader: 'babel',
        exclude: /node_modules/,
        query: DEBUG ? {
          plugins: [['react-transform', {
            transforms: [{
              transform: 'react-transform-hmr',
              imports: ['react'],
              locals: ['module'],
            }],
          }]],
        } : {},
      },
      // {
      //   test: /\.(eot|gif|jpe?g|png|svg|woff2?|ttf)$/,
      //   loader: `url?limit=10000&name=[name]${DEBUG ? '' : '.[hash:7]'}.[ext]`,
      //   exclude: /node_modules/,
      // },
    ],
  },
  plugins: [
    new AssetsPlugin({
      filename: 'assets.json',
      path: 'build/dist',
    }),
    new ExtractTextPlugin(`[name]${DEBUG ? '' : '.[hash]'}.css`),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(DEBUG ? 'development' : 'production'),
    }),
    ...DEBUG ? [
      new webpack.HotModuleReplacementPlugin(),
    ] : [
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false,
        },
      }),
    ],
  ],
  postcss: [
    autoprefixer({
      browsers: [
        '> 1%',
        'last 2 versions',
      ],
    }),
  ],
};
