/* eslint-env mocha */
/* eslint-disable prefer-arrow-callback, func-names */
import expect from 'expect';
import fetchMock from 'fetch-mock';
import configureStore from 'redux-mock-store';
import { middleware } from '../shared/configure-store';
import reducer, { like, unlike, LIKE, UNLIKE } from './likes';

const mockStore = configureStore(middleware);

describe('likes async', function () {
  before(function () {
    fetchMock
      .mock('/api/likes/id', 'POST', 200)
      .mock('/api/likes/fail', 'POST', 500)
      .mock('/api/likes/id', 'DELETE', 200)
      .mock('/api/likes/fail', 'DELETE', 500);
  });

  function testAction(action, expected, done) {
    const store = mockStore({});
    store
      .dispatch(action)
      .then(function () {
        expect(store.getActions()[0])
          .toEqual(expected);
      })
      .then(done)
      .catch(done);
  }

  it('should create an action with payload id on like', function (done) {
    testAction(like('id'), { type: LIKE, payload: 'id' }, done);
  });

  it('should create an action with payload false when like fails', function (done) {
    testAction(like('fail'), { type: LIKE, payload: false }, done);
  });

  it('should create an action with payload id on unlike', function (done) {
    testAction(unlike('id'), { type: UNLIKE, payload: 'id' }, done);
  });

  it('should create an action with payload false when unlike fails', function (done) {
    testAction(unlike('fail'), { type: UNLIKE, payload: false }, done);
  });
});

describe('likes reducer', function () {
  it('should reduce like actions to a map of ids', function () {
    let state = reducer({}, { type: LIKE, payload: 'id1' });
    state = reducer(state, { type: LIKE, payload: 'id2' });
    expect(state)
      .toIncludeKeys(['id1', 'id2']);
  });

  it('should remove unliked ids from the store', function () {
    let state = { id1: true, id2: true };
    state = reducer(state, { type: UNLIKE, payload: 'id1' });
    expect(state)
      .toIncludeKeys(['id2']);
    state = reducer(state, { type: UNLIKE, payload: 'id2' });
    expect(state).toEqual({});
  });
});
