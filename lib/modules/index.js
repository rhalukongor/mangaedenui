import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import mangas from './mangas';
import likes from './likes';
import currentManga from './manga';
import currentChapter from './chapter';

const reducer = combineReducers({
  routing: routerReducer,
  mangas,
  currentManga,
  likes,
  currentChapter,
});

export default reducer;
