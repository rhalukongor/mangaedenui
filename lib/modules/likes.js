import omit from 'ramda/src/omit';
import always from 'ramda/src/always';

export const LIKE = 'LIKES/LIKE';
export const UNLIKE = 'LIKES/UNLIKE';

const assign = Object.assign;

export function like(id) {
  return {
    type: LIKE,
    payload: fetch(`/api/likes/${id}`, {
      method: 'POST',
      mode: 'same-origin',
      credentials: 'same-origin',
    }).then(response => response.ok && id)
      .catch(always(false)),
  };
}

export function unlike(id) {
  return {
    type: UNLIKE,
    payload: fetch(`/api/likes/${id}`, {
      method: 'DELETE',
      mode: 'same-origin',
      credentials: 'same-origin',
    }).then(response => response.ok && id)
      .catch(always(false)),
  };
}

export default function reducer(state = {}, { type, payload }) {
  const mangaId = payload;
  switch (type) {

    case LIKE:
      return assign({}, state, { [mangaId]: true });

    case UNLIKE:
      return omit([mangaId], state);

    default:
      return state;
  }
}
