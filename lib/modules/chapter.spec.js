/* eslint-env mocha */
/* eslint-disable prefer-arrow-callback, func-names */
import expect from 'expect';
import reducer, { fetchChapterInfo, FETCH, clear } from './chapter';
import fetchMock from 'fetch-mock';
import configureStore from 'redux-mock-store';
import { HOST } from '../config';
import { middleware } from '../shared/configure-store';

const mockStore = configureStore(middleware);

describe('chapter action', function () {
  function chaptersResponse() {
    return {
      images: [
        [1, 'id1', 100, 100],
        [2, 'id2', 200, 200],
      ],
    };
  }

  before(function () {
    fetchMock
      .mock(new RegExp(`^${HOST}/chapter/.*`),
            chaptersResponse());
  });

  it('should dispatch a fetch action with response', function (done) {
    const store = mockStore({});
    const action = {
      type: FETCH,
      payload: chaptersResponse(),
    };
    store
      .dispatch(fetchChapterInfo('m1', 'c1'))
      .then(function () {
        expect(store.getActions()[0])
          .toEqual(action);
      })
      .then(done)
      .catch(done);
  });
});

describe('chapter reducer', function () {
  it('should set action payload after a fetch action', function () {
    expect(reducer({}, { type: FETCH, payload: 'test' }))
      .toEqual('test');
  });

  it('should set null after a clear action', function () {
    expect(reducer({}, clear()))
      .toBe(null);
  });
});
