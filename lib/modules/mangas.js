import { HOST } from '../config';
const assign = Object.assign;

export const FETCH = 'MANGAS/FETCH';
export const FILTER_BY_TITLE = 'MANGAS/FILTER_BY_TITLE';

export function fetchMangas() {
  return {
    type: FETCH,
    payload: fetch(`${HOST}/list/0/`)
      .then(response => response.json()),
  };
}

export function filterMangas(query) {
  return {
    type: FILTER_BY_TITLE,
    payload: query,
  };
}

function makeFilter(getterFn) {
  return (mangas, query) => {
    const exp = new RegExp(`.*${query}.*`, 'i');
    return mangas.filter(manga => exp.test(getterFn(manga)));
  };
}

function filterByTitle(mangas, title) {
  return makeFilter(({ t }) => t)(mangas, title);
}

function getSorter(sortBy) {
  switch (sortBy) {
    case 'title':
      return (a, b) => a.t - b.t;
    default: // Default to hits
      return (a, b) => b.h - a.h;
  }
}

const initialState = {
  sortBy: 'hits',
};

export default function reducer(state = initialState, action) {
  if (action.error) {
    return state;
  }

  const getList = ({ manga }) => manga;

  switch (action.type) {
    case FETCH:
      return assign(state, { list: getList(action.payload).sort(getSorter(state.sortBy)) });

    case FILTER_BY_TITLE:
      return assign(state, { filteredList: filterByTitle(state.list, action.payload) });

    default:
      return state;
  }
}
