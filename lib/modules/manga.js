import { HOST } from '../config';
export const FETCH = 'MANGA/FETCH';

export function fetchMangaInfo(mangaId) {
  return {
    type: FETCH,
    payload: fetch(`${HOST}/manga/${mangaId}`)
      .then(response => response.json()),
  };
}

const initialState = null;

export default function reducer(state = initialState,
                                { type, payload }) {
  switch (type) {
    case FETCH:
      return payload;
    default:
      return state;
  }
}
