import { HOST } from '../config';

export const FETCH = 'CHAPTER/FETCH';
export const CLEAR = 'CHAPTER/CLEAR';

export function fetchChapterInfo(mangaId, chapterId) {
  return {
    type: FETCH,
    payload: fetch(`${HOST}/chapter/${chapterId}`)
      .then(response => response.json()),
  };
}

export function clear() {
  return {
    type: CLEAR,
    payload: null,
  };
}

export default function reducer(state = null,
                                { type, payload }) {
  switch (type) {
    case FETCH:
      return payload;
    case CLEAR:
      return null;
    default:
      return state;
  }
}
