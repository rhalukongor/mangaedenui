import compression from 'compression';
import express from 'express';
import morgan from 'morgan';
import path from 'path';
import webpack from 'webpack';
import cookieParser from 'cookie-parser';

import config from '../../webpack.config.babel';
import reactMiddleware from './middleware/react-middleware';

const DEBUG = process.env.NODE_ENV !== 'production';
const PORT = process.env.PORT || 3000;
const server = express();

server.set('view engine', 'pug');
server.set('views', path.resolve(__dirname, 'views'));

server.use(compression());
server.use(cookieParser());
server.use(morgan(DEBUG ? 'dev' : 'combined'));

server.post('/api/likes/:mangaId', (req, res) => {
  const likes = req.cookies.likes || {};
  likes[req.params.mangaId] = 1;
  res.cookie('likes', likes);
  res.sendStatus(200);
});

server.delete('/api/likes/:mangaId', (req, res) => {
  const likes = req.cookies.likes || {};
  delete likes[req.params.mangaId];
  res.cookie('likes', likes);
  res.sendStatus(200);
});

if (DEBUG) {
  const compiler = webpack(config);
  /* eslint-disable global-require */
  const webpackHotMiddleware = require('webpack-hot-middleware');
  const webpackMiddleware = require('webpack-dev-middleware');
  /* eslint-enable global-require */

  server.use(webpackMiddleware(compiler, {
    historyApiFallback: true,
    hot: true,
    quiet: true,
  }));
  server.use(webpackHotMiddleware(compiler));
}

server.use(express.static(path.resolve(__dirname, '../../build/dist')));

server.use(reactMiddleware);

server.listen(PORT, () =>
  /* eslint-disable no-console */
  console.info(`Server running in ${server.get('env')} on port ${PORT}`)
);
