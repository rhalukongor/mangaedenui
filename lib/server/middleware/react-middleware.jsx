import React from 'react';
import { RouterContext, match } from 'react-router';
import { createStore } from 'redux';
import reducer from '../../modules';
import { Provider } from 'react-redux';
import { renderToString } from 'react-dom/server';
import configureStore from '../../shared/configure-store';
import getRoutes from '../../shared/routes';

export default function middleware({ url, cookies: { likes } }, res) {
  let initialState = likes ? { likes } : {};
  const store = configureStore(createStore)(reducer, initialState);
  const routes = getRoutes(store);
  match({ routes, location: url }, (error, redirectLocation, renderProps) => {
    if (error) {
      return res.status(500).send(error.message);
    } else if (redirectLocation) {
      return res.redirect(302, redirectLocation.pathname + redirectLocation.search);
    } else if (!renderProps) {
      return res.status(404).send('Not Found');
    }

    const assets = require('../../../build/dist/assets.json'); // eslint-disable-line global-require
    initialState = JSON.stringify(store.getState());
    const content = renderToString(
      <Provider store={store}>
        <RouterContext {...renderProps} />
      </Provider>
    );

    return res.render('index', {
      assets,
      content,
      initialState,
    });
  });
}
