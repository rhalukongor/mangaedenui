// TODO: Move this to config
export const PAGE_SIZE = 25;

export default function getPageBounds(page, numRecords) {
  const p = isNaN(page) ? 0 : page - 1;
  const min = Math.min(p * PAGE_SIZE, numRecords);
  const max = Math.min(((p + 1) * PAGE_SIZE), numRecords);
  return [min, max];
}

export function getNumPages(numRecords) {
  return Math.ceil(numRecords / PAGE_SIZE);
}
