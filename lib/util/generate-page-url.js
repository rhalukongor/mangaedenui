import url from 'url';

export default function ({ pathname, hash, query }) {
  return page => url.format({
    pathname,
    hash,
    query: Object.assign({}, query, { page }),
  });
}
