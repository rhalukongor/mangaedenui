/* eslint-disable react/prefer-stateless-function */
import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

export default class HomeLayout extends Component {
  render() {
    const { contentTop, contentMain, contentBottom } = this.props;
    return (
      <div className="container">
        <Link to="/">
          <h1 className="text-center">MangaEden</h1>
        </Link>
        <div className="row">
          <div>{contentTop}</div>
        </div>
        <div className="row">
          <div>{contentMain}</div>
        </div>
        {contentBottom && (
          <div className="row">
            <div>{contentBottom}</div>
          </div>)}
      </div>
    );
  }
}

HomeLayout.propTypes = {
  contentTop: PropTypes.node.isRequired,
  contentMain: PropTypes.node.isRequired,
  contentBottom: PropTypes.node,
};
