import React from 'react';
import { createStore } from 'redux';
import reducer from '../modules';
import { Provider } from 'react-redux';
import { render } from 'react-dom';
import { Router, browserHistory } from 'react-router';
import getRoutes from '../shared/routes';
import { syncHistoryWithStore } from 'react-router-redux';
import configureStore from '../shared/configure-store';

const initialState = window.__INITIAL_STATE__; // eslint-disable-line no-underscore-dangle
const store = configureStore(createStore)(reducer, initialState);
const history = syncHistoryWithStore(browserHistory, store);

render(
  <Provider store={store}>
    <Router history={history}>{getRoutes(store)}</Router>
  </Provider>,
  document.getElementById('root')
);

window.__INITIAL_STATE__ = null; // eslint-disable-line no-underscore-dangle
