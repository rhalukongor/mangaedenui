import { applyMiddleware } from 'redux';
import promiseMiddleware from 'redux-promise';
import { browserHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';
import thunkMiddleware from 'redux-thunk';
// import createLogger from 'redux-logger';

export const middleware = [
  thunkMiddleware,
  promiseMiddleware,
];

// TODO: This is not a good way to test runtime. Let's see how to envify in
// webpack
if (global.document) {
  middleware.push(routerMiddleware(browserHistory));
}

// middleware.push(createLogger())

export default function configureStore(createStore) {
  return applyMiddleware(...middleware)(createStore);
}
