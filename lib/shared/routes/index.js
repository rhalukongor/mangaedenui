import HomeLayout from '../../layouts/home';
import SearchForm from '../../containers/search-form';
import MangaList from '../../containers/manga-list';
import MangaListPagination from '../../containers/manga-list-pagination';
import FilteredMangaList from '../../containers/filtered-manga-list';
import FilteredMangaListPagination from '../../containers/filtered-manga-list-pagination';
import ChapterList from '../../containers/chapter-list';
import { fetchMangas, filterMangas } from '../../modules/mangas';
import { fetchMangaInfo } from '../../modules/manga';
import { fetchChapterInfo, clear } from '../../modules/chapter';
import nAry from 'ramda/src/nAry';

const nullary = nAry(0);

// We may or may not have fetched manga list. Provide a function always returns
// a promise
function makeDispatchers(store) {
  return () => {
    const { getState, dispatch } = store;
    const { mangas: { list } } = getState();
    if (list && list.length) {
      return new Promise(resolve => setTimeout(resolve(), 0));
    }

    return dispatch(fetchMangas());
  };
}

export default function getRoutes(store) {
  const { dispatch } = store;
  const maybeFetchMangas = makeDispatchers(store);
  return {
    path: '/',
    component: HomeLayout,
    onEnter: (nextState, replace, done) =>
      maybeFetchMangas()
      .then(nullary(done))
      .catch(done),
    indexRoute: {
      components: {
        contentTop: SearchForm,
        contentMain: MangaList,
        contentBottom: MangaListPagination,
      },
    },
    childRoutes: [{
      path: 'filter/:query',
      components: {
        contentTop: SearchForm,
        contentMain: FilteredMangaList,
        contentBottom: FilteredMangaListPagination,
      },
      onEnter: (nextState, replace, done) => {
        const { params: { query } } = nextState;
        maybeFetchMangas()
          .then(action => {
            if (action) {
              dispatch(action);
            }
            return dispatch(filterMangas(query));
          })
          .then(nullary(done))
          .catch(done);
      },
    }, {
      path: 'manga/:id',
      components: {
        contentTop: SearchForm,
        contentMain: ChapterList,
      },
      onEnter: (nextState, replace, done) => {
        const { params: { id } } = nextState;
        dispatch(fetchMangaInfo(id))
          .then(nullary(done))
          .catch(done);
      },
      childRoutes: [{
        path: 'chapters/:chapterId',
        onEnter: (nextState, replace, done) => {
          const { params: { id, chapterId } } = nextState;
          dispatch(fetchChapterInfo(id, chapterId))
            .then(nullary(done))
            .catch(done);
        },
        onLeave: () => dispatch(clear()),
        childRoutes: [{
          path: ':pageId',
        }],
      }],
    }],
  };
}
