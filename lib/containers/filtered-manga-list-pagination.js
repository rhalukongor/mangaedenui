import { connect } from 'react-redux';
import Pagination from '../components/pagination';
import { getNumPages } from '../util/get-page-bounds';
import generatePageURL from '../util/generate-page-url';

function select({ mangas: { filteredList } }, { location }) {
  const { query: { page } } = location;

  return {
    currentPage: parseInt(page, 10) || 1,
    totalPages: getNumPages(filteredList.length),
    generateURL: generatePageURL(location),
  };
}

export default connect(select)(Pagination);
