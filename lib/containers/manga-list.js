import { connect } from 'react-redux';
import MangaList from '../components/manga-list';
import getPageBounds from '../util/get-page-bounds';

function select({ mangas: { list }, likes },
                { location: { query: { page } } }) {
  const [min, max] = getPageBounds(page, list.length);
  const appendLikes = manga =>
          Object.assign({}, manga, { liked: manga.i in likes });
  return {
    list: list
      .slice(min, max)
      .map(appendLikes),
  };
}

export default connect(select)(MangaList);
