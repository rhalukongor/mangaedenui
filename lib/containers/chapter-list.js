import { connect } from 'react-redux';
import ChapterList from '../components/chapter-list';
import { CDN_URI } from '../config';
import { push } from 'react-router-redux';
import pipe from 'ramda/src/pipe';
import omit from 'ramda/src/omit';

const assign = Object.assign;

function mapImages([, url, width, height]) {
  return { src: `${CDN_URI}/${url}`, width, height };
}

function getPageURL({ id, chapterId, pageId }) {
  let url = `/manga/${id}`;
  if (chapterId) url = `${url}/chapters/${chapterId}`;
  if (pageId) url = `${url}/${pageId}`;
  return url;
}

function makeNewPageURL(mod) {
  return params => {
    let { pageId } = params;
    pageId = mod(parseInt(pageId, 10)).toString();
    return getPageURL(assign({}, params, { pageId }));
  };
}

const nextPage = pipe(makeNewPageURL(x => x + 1), push);
const prevPage = pipe(makeNewPageURL(x => x - 1), push);
const back = pipe(omit(['pageId', 'chapterId']),
                  getPageURL, push);
const noop = () => {};

function select({ currentManga, currentChapter },
                { params: { id, chapterId, pageId } }) {
  return assign({}, currentManga, {
    id,
    currentChapter: chapterId && currentChapter &&
      currentChapter.images.map(mapImages),
    pageId: chapterId && pageId,
  });
}

function mapDispatch(dispatch, { params }) {
  if (!params.chapterId) {
    return {
      onNextPage: noop,
      onPrevPage: noop,
      onClose: noop,
    };
  }

  let p;
  if (params.pageId) {
    p = params;
  } else {
    p = assign({ pageId: '1' }, params);
  }

  const nextPageAction = nextPage(p);
  const prevPageAction = prevPage(p);
  const upPageAction = back(p);

  return {
    onNextPage: dispatch.bind(null, nextPageAction),
    onPrevPage: dispatch.bind(null, prevPageAction),
    onClose: dispatch.bind(null, upPageAction),
  };
}

export default connect(select, mapDispatch)(ChapterList);
