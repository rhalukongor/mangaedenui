import { connect } from 'react-redux';
import SearchForm from '../components/search-form';
import { push } from 'react-router-redux';

function mapDispatch(dispatch) {
  return {
    onSubmit: query => dispatch(push(`/filter/${query}`)),
  };
}

export default connect(null, mapDispatch)(SearchForm);
