import { connect } from 'react-redux';
import MangaList from '../components/manga-list';
import getPageBounds from '../util/get-page-bounds';

function select({ mangas: { filteredList }, likes },
                { location: { query: { page } } }) {
  const [min, max] = getPageBounds(page, filteredList.length);
  const appendLikes = manga =>
          Object.assign({}, manga, { liked: manga.i in likes });
  return {
    list: filteredList
      .slice(min, max)
      .map(appendLikes),
  };
}

export default connect(select)(MangaList);
