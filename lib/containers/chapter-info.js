import { connect } from 'react-redux';
import ChapterInfo from '../components/chapter-info';
import { like, unlike } from '../modules/likes';

function select({ likes }, { id }) {
  return {
    liked: id in likes,
  };
}

function mapDispatch(dispatch, { id }) {
  return {
    like: () => dispatch(like(id))
      .then(action => action.payload),
    unlike: () => dispatch(unlike(id))
      .then(action => action.payload),
  };
}

export default connect(select, mapDispatch)(ChapterInfo);
