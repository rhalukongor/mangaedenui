/* eslint-disable react/prefer-stateless-function */
import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import styles from './manga-list.css';
import cssModules from 'react-css-modules';
import formatNum from 'format-number';
import { CDN_URI } from '../config';

export class MangaList extends Component {
  getImageURL(url) {
    return `${CDN_URI}/125x/${url}`;
  }

  render() {
    const { list } = this.props;

    const renderLink = (id, content) => (
      <Link to={`/manga/${id}`}>
        {content}
      </Link>
    );

    const format = formatNum();
    const renderItem = item => (
      <tr key={item.i}>
        <td>{renderLink(item.i, (
          <img styleName="image" alt={item.t} src={this.getImageURL(item.im)} />
          ))}</td>
        <td>
          {renderLink(item.i, item.t)}
          {item.liked && (<span styleName="liked">{item.liked && '❤'}</span>)}
        </td>
        <td>{format(item.h)}</td>
      </tr>
    );

    return (
      <table className="table" styleName="root" >
        <thead>
          <tr>
            <th></th>
            <th>Manga</th>
            <th>Görüntülenme</th>
          </tr>
        </thead>
        <tbody>
          {list.map(renderItem)}
        </tbody>
      </table>
    );
  }
}

MangaList.propTypes = {
  list: PropTypes.array.isRequired,
};

export default cssModules(styles)(MangaList);
