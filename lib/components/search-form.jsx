/* eslint-disable react/prefer-stateless-function */
import React, { Component, PropTypes } from 'react';
import styles from './search-form.css';
import cssModules from 'react-css-modules';

class SearchForm extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    if (typeof this.getQuery === 'function') {
      const query = this.getQuery();
      if (query) {
        const { onSubmit } = this.props;
        if (typeof onSubmit === 'function') {
          onSubmit(query);
        }
      }
    }
  }

  render() {
    const handleSubmit = this.handleSubmit;
    const setQueryInput = input => {
      this.getQuery = () => input.value;
    };
    return (
      <div className="col-sm-12">
        <form styleName="form" onSubmit={handleSubmit}>
          <div className="form-group" styleName="group">
            <label labelFor="search-input" styleName="label">Manga Ara:</label>
            <input
              id="search-input" styleName="input" type="text"
              placeholder="Örn: Fuuka" ref={setQueryInput}
            />
          </div>
          <button className="btn" styleName="search-button">Ara</button>
        </form>
      </div>
    );
  }
}

SearchForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default cssModules(styles)(SearchForm);
