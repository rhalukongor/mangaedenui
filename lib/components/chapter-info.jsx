/* eslint-disable react/prefer-stateless-function */
import React, { Component, PropTypes } from 'react';
import styles from './chapter-info.css';
import cssModules from 'react-css-modules';
import formatNum from 'format-number';
import { CDN_URI } from '../config';

class ChapterInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      liked: props.liked,
      processingLike: false,
    };
  }

  tryLike() {
    const { like } = this.props;
    if (typeof like === 'function') {
      like()
        .then(res => this.setState({
          processingLike: false,
          liked: res,
        }));
    }
    this.setState({ processingLike: true });
  }

  tryUnlike() {
    const { unlike } = this.props;
    if (typeof unlike === 'function') {
      unlike()
        .then(res => this.setState({
          processingLike: false,
          liked: !res,
        }));
    }
    this.setState({ processingLike: true });
  }

  renderCategory(cat) {
    return (
      <span key={cat} styleName="category-item">{cat}</span>
    );
  }

  renderProp([key, value]) {
    return (
      <tr styleName="details-list-item" key={key}>
        <th styleName="details-list-item-label">
          {key}
        </th>
        <td styleName="details-list-item-text">
          {value}
        </td>
      </tr>
    );
  }

  render() {
    const { title, image, description, categories,
            author, hits, released, chapters_len } = this.props;
    const { liked, processingLike } = this.state;
    const toggleLike = liked ? this.tryUnlike.bind(this) :
                       this.tryLike.bind(this);
    const format = formatNum();
    const renderCategory = this.renderCategory.bind(this);
    const renderProp = this.renderProp.bind(this);
    const chaptersLen = chapters_len; // eslint-disable-line camelcase
    const tableProps = [
      ['Yazar', author],
      ['Görüntülenme', format(hits)],
      ['Yayınlanma', released],
      ['Bölüm sayısı', chaptersLen],
    ];
    return (
      <div styleName="root">
        <img
          styleName="banner"
          src={`${CDN_URI}/200x/${image}`}
          alt={title}
        />
        <div>
          <button
            type="button" className="btn"
            disabled={processingLike}
            styleName="like-button" onClick={toggleLike}
          >{liked ? 'Beğendin' : 'Beğen'}</button>
          <h3 styleName="title">{title}</h3>
        </div>
        <p styleName="description">{description}</p>
        <div styleName="category-list">
          {categories.map(renderCategory)}
        </div>
        <table styleName="details-list">
          <tbody>
            {tableProps.map(renderProp)}
          </tbody>
        </table>
      </div>
    );
  }
}

ChapterInfo.propTypes = {
  title: PropTypes.string.isRequired,
  image: PropTypes.string,
  description: PropTypes.string,
  categories: PropTypes.arrayOf(PropTypes.string),
  author: PropTypes.string.isRequired,
  hits: PropTypes.number.isRequired,
  released: PropTypes.number.isRequired,
  chapters_len: PropTypes.number.isRequired,
  liked: PropTypes.bool.isRequired,
  like: PropTypes.func.isRequired,
  unlike: PropTypes.func.isRequired,
};

export default cssModules(styles)(ChapterInfo);
