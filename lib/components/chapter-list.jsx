/* eslint-disable react/prefer-stateless-function */
import React, { Component, PropTypes } from 'react';
import ChapterInfo from '../containers/chapter-info';
import styles from './chapter-list.css';
import cssModules from 'react-css-modules';
import { Link } from 'react-router';
import moment from 'moment';
import omit from 'ramda/src/omit';
import LightBox from 'react-images';

class ChapterList extends Component {

  bindRenderChapter(mangaId) {
    return ([no, date, title, id]) =>
      this.renderChapter.call(
        this, mangaId, no, date, title, `/manga/${mangaId}/chapters/${id}`);
  }

  renderChapter(mangaId, no, date, title, url) {
    const sTitle = title || '';
    const sep = title ? ': ' : '';
    return (
      <tr styleName="row" key={url}>
        <td styleName="title">
          <Link to={url}>
            {`Bölüm ${no}${sep}${sTitle}`}
          </Link>
        </td>
        <td styleName="date">
          {moment.unix(date).format('DD/MM/YYYY')}
        </td>
      </tr>
    );
  }

  render() {
    const { id, chapters, currentChapter,
            onNextPage, onPrevPage, onClose } = this.props;
    const pageId = isNaN(this.props.pageId) ? 0 :
                   this.props.pageId - 1;
    const infoProps = omit(['styles'], this.props);
    const renderChapter = this.bindRenderChapter(id);
    return (
      <div styleName="root" className="col-sm-12">
        <LightBox
          images={currentChapter || []}
          currentImage={pageId}
          isOpen={currentChapter &&
                  currentChapter.length}
          onClickNext={onNextPage}
          onClickPrev={onPrevPage}
          onClose={onClose}
        />
        <ChapterInfo { ...infoProps } />
        <hr />
        <table className="table">
          <thead>
            <tr>
              <th>Bölüm</th>
              <th>Eklendiği tarih</th>
            </tr>
          </thead>
          <tbody>{chapters.map(renderChapter)}</tbody>
        </table>
      </div>
    );
  }
}

ChapterList.propTypes = Object.assign({
  id: PropTypes.string.isRequired,
  chapters: PropTypes.arrayOf(PropTypes.array),
  currentChapter: PropTypes.array,
  onNextPage: PropTypes.func.isRequired,
  onPrevPage: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
}, ChapterInfo.propTypes);

export default cssModules(styles)(ChapterList);
