/* eslint-env mocha */
import React from 'react';
import expect from 'expect';
import expectJSX from 'expect-jsx';
import { createRenderer } from 'react-addons-test-utils';
import { MangaList } from './manga-list.jsx';
import { Link } from 'react-router';

expect.extend(expectJSX);

describe('MangaList', () => {
  it('should list mangas', () => {
    const list = [
      { i: 'id1', t: 'title1', h: 100, im: 'img1', liked: true },
      { i: 'id2', t: 'title2', h: 200, im: 'img2' },
    ];
    const renderer = createRenderer();
    renderer.render(<MangaList list={list} />);
    expect(renderer.getRenderOutput())
      .toEqualJSX(<table styleName="root" className="table">
        <thead>
          <tr>
            <th></th>
            <th>Manga</th>
            <th>Görüntülenme</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <Link to="/manga/id1">
                <img
                  src="//cdn.mangaeden.com/mangasimg/125x/img1"
                  alt="title1"
                  styleName="image"
                />
              </Link>
            </td>
            <td>
              <Link to="/manga/id1">title1</Link>
              <span styleName="liked">❤</span>
            </td>
            <td>100</td>
          </tr>
          <tr>
            <td>
              <Link to="/manga/id2">
                <img
                  src="//cdn.mangaeden.com/mangasimg/125x/img2"
                  alt="title2"
                  styleName="image"
                />
              </Link>
            </td>
            <td>
              <Link to="/manga/id2">title2</Link>
            </td>
            <td>200</td>
          </tr>
        </tbody>
      </table>);
  });
});
