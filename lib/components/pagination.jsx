import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import styles from './pagination.css';
import cssModules from 'react-css-modules';

function Pagination({ currentPage, totalPages, limit, generateURL }) {
  if (totalPages < 2) return null;

  const hLimit = Math.ceil(limit / 2);
  const [min, max] = [currentPage - hLimit,
                      currentPage + hLimit]
  .map(num => Math.max(1, Math.min(totalPages, num)));

  const links = new Array(max - min + 1);

  for (let i = min; i <= max; i++) {
    const url = generateURL(i);
    links[i - 1] = (
      <li
        key={url}
        styleName="item"
        className={i === currentPage && 'active'}
      >
        <Link to={url}>{i}</Link>
      </li>
    );
  }

  return (
    <nav styleName="root">
      <ul styleName="list" className="pagination">
        <li className={currentPage < 2 && 'disabled'}>
          <Link to={generateURL(currentPage - 1)} ariaLabel="Önceki">
            <span ariaHidden="true">«</span></Link></li>
        {links}
        <li className={totalPages - 1 < currentPage && 'disabled'}>
          <Link to={generateURL(currentPage + 1)} ariaLabel="Sonraki">
            <span ariaHidden="true">»</span></Link></li>
      </ul>
    </nav>
  );
}

Pagination.propTypes = {
  limit: PropTypes.number,
  currentPage: PropTypes.number.isRequired,
  totalPages: PropTypes.number.isRequired,
  generateURL: PropTypes.func.isRequired,
};

Pagination.defaultProps = {
  limit: 5,
};

export default cssModules(styles)(Pagination);
