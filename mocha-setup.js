import hook from 'css-modules-require-hook';
import fetch from 'node-fetch';

global.fetch = fetch;

hook({
  generateScopedName: '[name]__[local]',
  mode: 'local',
  rootDir: './lib/modules',
});
